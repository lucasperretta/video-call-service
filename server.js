const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server, { 'pingInterval': 5000, 'pingTimeout': 10000 })
const { v4: uuidV4 } = require('uuid')

app.set('view engine', 'ejs')
app.use(express.static('public'))

app.get('/', (req, res) => {
    res.redirect(`/${uuidV4()}`)
})

app.get('/:room', (req, res) => {
    res.render('room', {
        roomId: req.params.room,
        serverURL: process.env.SERVER_URL ? process.env.SERVER_URL : '/',
        peerURL: process.env.PEER_URL ? process.env.PEER_URL : '/',
        peerPort: process.env.PEER_PORT ? process.env.PEER_PORT : '3001'
    })
})

io.on('connection', socket => {
    socket.on('join-room', (roomId, userId) => {
        socket.join(roomId)
        setTimeout(() => {
            socket.to(roomId).broadcast.emit('user-connected', userId)
        }, 0)

        socket.once('disconnect', () => {
            socket.to(roomId).broadcast.emit('user-disconnected', userId)
        })

        socket.on('track-changed', (userId, track, enabled) => {
            socket.to(roomId).broadcast.emit('user-track-changed', userId, track, enabled)
        })

        socket.on('user-properties', (properties) => {
            socket.to(roomId).broadcast.emit('user-properties', userId, properties)
        })
    })
})

server.listen(3000)