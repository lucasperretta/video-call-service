FROM node:12-alpine

WORKDIR /app

RUN npm i -g peer

VOLUME /app/data

CMD peerjs --port 3001
