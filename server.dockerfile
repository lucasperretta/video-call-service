FROM node:12-alpine

WORKDIR /app

COPY . /app/
RUN npm install

VOLUME /app/data

CMD node server.js
