const connectedSound = new Audio('connected_sound.wav')
const disconnectedSound = new Audio('disconnected_sound.wav')

const socket = io(SERVER_URL)
const videoGrid = document.getElementById('video-grid')
const myPeer = new Peer(undefined, {
    host: PEER_URL,
    port: PEER_PORT
})

const mainVideo = document.getElementById('main-video')
mainVideo.muted = true
if (!document.pictureInPictureEnabled || mainVideo.disablePictureInPicture) {
    document.getElementById('pip-button').remove();
}

const myVideoElement = createNewVideo(true)
const myVideo = myVideoElement.getElementById('video')
const myMicIndicator = myVideoElement.getElementById('mic')
const myVidIndicator = myVideoElement.getElementById('vid')
myVideo.muted = true
myVideo.style.transform = "scaleX(-1)"

const screenShare = new URLSearchParams(window.location.search).get('screenShare')

let name = window.localStorage.userName != undefined ? window.localStorage.userName : 'Usuario Invitado'
window.localStorage.userName = name
let myProperties = {
    micEnabled: true,
    vidEnabled: true,
    name: name,
    screenShare: screenShare,
}

const peers = {}
let callJoinRoomSet = false
if (screenShare) {
    window.document.title = "Compartiendo Pantalla"
}
let media = screenShare ? navigator.mediaDevices.getDisplayMedia({
    video: true,
    audio: true
}) : navigator.mediaDevices.getUserMedia({
    video: true,
    audio: true
})
media.then(stream => {
    connectToCall(stream)
}).catch(error => {
    connectToCall(null)
})

function connectToCall(stream) {
    if (stream != null) {
        addVideoStream(myVideoElement, stream)
        setMainVideo(stream, true)
    }
    callJoinRoom()

    myPeer.on('call', call => {
        call.answer(stream)
        let videoElement = createNewVideo(false)
        let components = getVideoElementComponents(videoElement)
        call.on('stream', (userVideoStream) => {
            addVideoStream(videoElement, userVideoStream, call.peer)
            emitProperties()
        })
        call.on('close', () => {
            if (mainVideo.srcObject == components.video.srcObject) {
                mainVideo.srcObject = null
                selectedMainVideo = null
            }
            components.videoDiv.remove()
            disconnectedSound.play()
        })
        peers[call.peer] = call
        peers[call.peer].components = components
    })

    socket.on('user-connected', (userId) => {
        connectToNewUser(userId, stream)
    })
}

function connectToNewUser(userId, stream) {
    let call = myPeer.call(userId, stream)
    let videoElement = createNewVideo(false)
    let components = getVideoElementComponents(videoElement)
    call.on('stream', userVideoStream => {
        addVideoStream(videoElement, userVideoStream, call.peer)
        emitProperties()
    })
    call.on('close', () => {
        if (mainVideo.srcObject == components.video.srcObject) {
            mainVideo.srcObject = null
            selectedMainVideo = null
        }
        components.videoDiv.remove()
        disconnectedSound.play()
    })
    peers[userId] = call
    peers[call.peer].components = components
}

function getVideoElementComponents(videoElement) {
    return {
        videoDiv: videoElement.getElementById('elements'),
        micIndicator: videoElement.getElementById('mic'),
        vidIndicator: videoElement.getElementById('vid'),
        video: videoElement.getElementById('video'),
    }
}

function emitProperties() {
    socket.emit('user-properties', myProperties)
}

socket.on('user-properties', (userId, properties) => {
    if (properties.screenShare == myPeer.id) {
        peers[userId].close()
        delete peers[userId]
        return
    }
    console.log(properties)
    peers[userId]['properties'] = properties

    if (properties.micEnabled) {
        peers[userId].components.micIndicator.classList.add('display-none')
    } else {
        peers[userId].components.micIndicator.classList.remove('display-none')
    }
    if (properties.vidEnabled) {
        peers[userId].components.vidIndicator.classList.add('display-none')
    } else {
        peers[userId].components.vidIndicator.classList.remove('display-none')
    }
    if (properties.screenShare) {
        setTimeout(() => {
            setMainVideo(peers[userId].videoElement.srcObject)
            selectedMainVideo = peers[userId].videoElement.srcObject
        }, 1000)
    }
})

socket.on('user-disconnected', userId => {
    if (peers[userId]) peers[userId].close()
    delete peers[userId]
})

myPeer.on('open', id => {
    callJoinRoom()
})

let areControlsVisible = false
let controlsVisibilityTimeout = null
document.addEventListener('mousemove', showControls, false)
document.addEventListener('mousedown', showControls)
document.addEventListener("keypress", (event) => {
    showControls()
    if (event.key == 'm') toggleMediaStreamTrack('audio')
    if (event.key == 'v') toggleMediaStreamTrack('video')
    if (event.key == 's') newShareScreenTab()
    if (event.key == 'e') endCall()
    if (event.key == 'f') fullScreen()
    if (event.key == 'h') toggleVideoGrid()
    if (event.key == 'p') togglePiP()
    if (event.key == 'l') {
        let enable = !myProperties.vidEnabled && !myProperties.micEnabled
        toggleMediaStreamTrack('audio', enable)
        toggleMediaStreamTrack('video', enable)
    }
})
document.addEventListener('mouseout', function (evt) {
    clearTimeout(controlsVisibilityTimeout)
    toggleControls(false)
});
document.addEventListener("keydown", (event) => {
    showControls()
    if (event.key == ' ') toggleMediaStreamTrack('audio', true)
})
document.addEventListener("keyup", (event) => {
    showControls()
    if (event.key == ' ') toggleMediaStreamTrack('audio', false)
})
mainVideo.addEventListener("dblclick", (event) => {
    fullScreen()
})
mainVideo.addEventListener("click", (event) => {
    clearTimeout(controlsVisibilityTimeout)
    toggleControls()
})

function showControls() {
    toggleControls(true)
    if (controlsVisibilityTimeout != null) {
        clearTimeout(controlsVisibilityTimeout)
        controlsVisibilityTimeout = null
    }
    controlsVisibilityTimeout = setTimeout(() => {
        controlsVisibilityTimeout = null
        toggleControls(false)
    }, window.matchMedia("(max-width:600px)").matches ? 3000 : 1400)
}

function toggleControls(visible) {
    if (areControlsVisible == visible) return
    areControlsVisible = visible
    let mainControls = document.getElementById('main-controls')
    let secondaryControls = document.getElementById('secondary-controls')
    if (visible) {
        document.body.classList.remove('no-cursor')
        mainControls.classList.remove('invisible')
        secondaryControls.classList.remove('invisible')
    } else {
        document.body.classList.add('no-cursor')
        mainControls.classList.add('invisible')
        secondaryControls.classList.add('invisible')
    }
}

function callJoinRoom() {
    if (!callJoinRoomSet) {
        callJoinRoomSet = true
        return
    }
    socket.emit('join-room', ROOM_ID, myPeer.id)
}

function addVideoStream(videoElement, stream, userId) {
    if (screenShare) return
    let video = videoElement.getElementById('video')
    if (video == null) {
        return
    }
    connectedSound.play()
    video.srcObject = stream
    video.addEventListener('loadedmetadata', () => {
        var userAgent = window.navigator.userAgent

        if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
            video.muted = true
            video.playsInline = true
            video.autoplay = true
        }
        try {
            video.play()
        } catch (error) { }
    })
    videoGrid.append(videoElement)
    if (userId !== undefined) {
        peers[userId]['videoElement'] = video
    }
    detectAudioVolume(stream, userId === undefined)
}

function toggleMediaStreamTrack(track, enable) {
    let tracks = myVideo.srcObject.getTracks()
    let enabled
    tracks.forEach(element => {
        if (element.kind == track) {
            element.enabled = enable != undefined ? enable : !element.enabled
            enabled = element.enabled
        }
    })
    let trackIndicator = null
    switch (track) {
        case 'audio':
            myProperties.micEnabled = enabled
            trackIndicator = myMicIndicator
            document.getElementById(enabled ? 'mic-on' : 'mic-off').classList.remove('display-none')
            document.getElementById(enabled ? 'mic-off' : 'mic-on').classList.add('display-none')
            break
        case 'video':
            myProperties.vidEnabled = enabled
            trackIndicator = myVidIndicator
            document.getElementById(enabled ? 'vid-on' : 'vid-off').classList.remove('display-none')
            document.getElementById(enabled ? 'vid-off' : 'vid-on').classList.add('display-none')
            break
    }
    emitProperties()
    if (enabled) {
        trackIndicator.classList.add('display-none')
    } else {
        trackIndicator.classList.remove('display-none')
    }
}

function newShareScreenTab() {
    let win = window.open(window.location.href + '?screenShare=' + myPeer.id, '_blank')
    win.focus()
}

function fullScreen() {
    if (document.fullscreenElement != null)
        document.exitFullscreen()
    else
        document.body.requestFullscreen()
}

let videoGridVisible = true
function toggleVideoGrid() {
    if (videoGridVisible) {
        videoGrid.classList.add('video-grid-hidden')
    } else {
        videoGrid.classList.remove('video-grid-hidden')
    }
    videoGridVisible = !videoGridVisible
}

function togglePiP() {
    if (!document.pictureInPictureElement) {
        mainVideo.requestPictureInPicture().catch(error => {});
        return;
    }
    document.exitPictureInPicture().catch(error => {});
}

let audioDetectionNextInLine = null;
let audioDetectionTalkTime = null;
function detectAudioVolume(stream, isUser) {
    if (isUser) return
    let audioContext = new AudioContext()
    let analyser = audioContext.createAnalyser()
    let microphone = audioContext.createMediaStreamSource(stream)
    let javascriptNode = audioContext.createScriptProcessor(2048, 1, 1)

    analyser.smoothingTimeConstant = 0.8
    analyser.fftSize = 1024

    microphone.connect(analyser)
    analyser.connect(javascriptNode)
    javascriptNode.connect(audioContext.destination)
    javascriptNode.onaudioprocess = function () {
        let array = new Uint8Array(analyser.frequencyBinCount)
        analyser.getByteFrequencyData(array)
        let values = 0

        let length = array.length
        for (var i = 0; i < length; i++) {
            values += (array[i])
        }

        let average = values / length

        if (average > 22) {
            if (audioDetectionNextInLine != stream) {
                audioDetectionNextInLine = stream
                audioDetectionTalkTime = new Date()
                return
            }
            let horaActual = new Date();
            if (horaActual.getTime() - audioDetectionTalkTime.getTime() < 200) return
            if (selectedMainVideo != null) return
            setMainVideo(stream, false)
        }
    }
}

function createNewVideo(isUser) {
    let element = document.getElementById('mini-video-template').content.cloneNode(true)
    let video = element.getElementById('video')
    video.dataset.isUser = isUser
    return element
}

function selectMainVideo(video) {
    selectedMainVideo = (selectedMainVideo == video.srcObject) ? null : video.srcObject
    let isUser = video.dataset.isUser == 'true'
    setMainVideo(video.srcObject, isUser)
}

let selectedMainVideo = null
function setMainVideo(stream, isUser) {
    if (mainVideo.srcObject == stream || screenShare) return
    mainVideo.srcObject = stream
    mainVideo.muted = true //isUser
    mainVideo.style.transform = isUser ? "scaleX(-1)" : null
    mainVideo.play()
}

function endCall() {
    toggleMediaStreamTrack('audio', false)
    toggleMediaStreamTrack('video', false)
    disconnectedSound.play()
    setTimeout(() => {
        window.open('https:\/\/google.com', '_self')
    }, 100)
}
